Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.0.2](https://bitbucket.org/java-universum/websocket-adapter/wiki/version/1.x) ###
> 26.04.2020

- Regular **maintenance**.

### [1.0.1](https://bitbucket.org/java-universum/websocket-adapter/wiki/version/1.x) ###
> 09.09.2019

- Build with the latest **Gradle 5.6** version.

### [1.0.0](https://bitbucket.org/java-universum/websocket-adapter/wiki/version/1.x) ###
> 18.05.2017

- First production release.