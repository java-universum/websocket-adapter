WebSocket Adapter
===============

[![CircleCI](https://circleci.com/bb/java-universum/websocket-adapter.svg?style=shield)](https://circleci.com/bb/java-universum/websocket-adapter)
[![Codecov](https://codecov.io/bb/java-universum/websocket-adapter/branch/main/graph/badge.svg)](https://codecov.io/bb/java-universum/websocket-adapter)
[![Codacy](https://api.codacy.com/project/badge/Grade/e89595f198d0478eab8760b18100b350)](https://www.codacy.com/app/universum-studios/websocket-adapter?utm_source=java-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=java-universum/websocket-adapter&amp;utm_campaign=Badge_Grade)
[![Java](https://img.shields.io/badge/java-1.8-blue.svg)](https://java.com)

## ! OBSOLETE ! ##

**> This project has become obsolete and will be no longer maintained. <**

---

Socket implementation that wraps asynchronous nature of WebSocket.

For more information please visit the **[Wiki](https://github.com/universum-studios/websocket_adapter/wiki)**.

## Download ##

Download the latest **[release](https://bitbucket.org/java-universum/websocket-adapter/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios:websocket-adapter:${DESIRED_VERSION}"

## [License](https://bitbucket.org/java-universum/websocket-adapter/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.